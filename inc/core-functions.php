<?php

if ( ! function_exists('slim_extra_features') ) {

// Register Theme Features
  function slim_extra_features()  {

	// Add theme support for Automatic Feed Links
   add_theme_support( 'automatic-feed-links' );

	// Set content width value based on the theme's design
   if ( ! isset( $content_width ) ) {
    $content_width = 1280;
  }

	// Add theme support for Title Tags
  add_theme_support( 'title-tag' );

	// Add theme support for Post Formats
  add_theme_support( 'post-formats', array( 'status', 'quote', 'gallery', 'image', 'video', 'link' ) );

	// Add theme support for Featured Images
  add_theme_support( 'post-thumbnails', array( 'post', ' page' ) );

	// Add theme support for Custom Background
  $background_args = array(
    'default-color'          => 'f3f3f3',
		// 'default-image'          => '',
    'default-repeat'         => 'repeat',
    'default-position-x'     => 'scroll',
		// 'wp-head-callback'       => '',
		// 'admin-head-callback'    => '',
		// 'admin-preview-callback' => '',
  );
  add_theme_support( 'custom-background', $background_args );

	// Custom logo support
  add_theme_support( 'custom-logo', array(
    // 'height'      => 120,
    // 'width'       => 300,
   'flex-height' => true,
   'flex-width'  => true,
   'header-text' => array( 'site-title', 'site-description' ),
 ) );

	// Add theme support for HTML5 Semantic Markup
  add_theme_support( 'html5', array( 'search-form', 'gallery', 'caption' ) );

	// Add theme support for Translation
  load_theme_textdomain( 'slim', get_template_directory() . '/language' );
}
add_action( 'after_setup_theme', 'slim_extra_features' );

}

if ( ! function_exists( 'slim_nav_menus' ) ) {

// Register Navigation Menus
  function slim_nav_menus() {

   $locations = array(
    'main-menu' => __( 'Header menu area', 'slim' ),
    'footer-menu' => __( 'Footer menu area', 'slim' ),
  );
   register_nav_menus( $locations );

 }
 add_action( 'init', 'slim_nav_menus' );

}

if ( ! function_exists( 'slim_sidebars' ) ) {

// Register Sidebars
  function slim_sidebars() {

   $args = array(
    'id' 						=> 'footer',
    'class'         => 'footer-widgets',
    'name'          => __( 'Footer', 'slim' ),
    'before_title'  => '<h4 class="widget-title">',
    'after_title'   => '</h4>',
  );
   register_sidebar( $args );

 }
 add_action( 'widgets_init', 'slim_sidebars' );

}

// Register Style
function additional_styles() {
	wp_register_style( 'core', get_template_directory_uri() . '/assets/css/core.css', false, false );
	wp_enqueue_style( 'core' );
}
add_action( 'wp_enqueue_scripts', 'additional_styles' );

// HTML5 style image markup
function html5_insert_image($html, $id, $caption, $title, $align, $url, $size) {
  $src = wp_get_attachment_image_src( $id, $size, false );
  $srclarge = wp_get_attachment_image_src( $id, large, false );
  $srcmedium = wp_get_attachment_image_src( $id, medium, false );
  $srcsmall = wp_get_attachment_image_src( $id, thumbnail, false );
  $html5 = "<figure id='post-$id media-$id' class='align$align'>";
  $html5 .= "<img src='$src[0]' srcset='";
  $html5 .= "$srcsmall[0] 320w, ";
  $html5 .= "$srcmedium[0] 640w, ";
  $html5 .= "$srclarge[0] 1100w";
  $html5 .= "' alt='$title' />";
  $html5 .= "<figcaption>$caption</figcaption>";
  $html5 .= "</figure>";
  return $html5;
}
add_filter( 'image_send_to_editor', 'html5_insert_image', 10, 9 );

/**
* Disable the emoji's
*/
function disable_emojis() {
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_action( 'admin_print_styles', 'print_emoji_styles' ); 
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' ); 
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
  add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
}
add_action( 'init', 'disable_emojis' );

/**
* Filter function used to remove the tinymce emoji plugin.
* 
* @param array $plugins 
* @return array Difference betwen the two arrays
*/
function disable_emojis_tinymce( $plugins ) {
  if ( is_array( $plugins ) ) {
    return array_diff( $plugins, array( 'wpemoji' ) );
  } else {
    return array();
  }
}

/**
* Remove emoji CDN hostname from DNS prefetching hints.
*
* @param array $urls URLs to print for resource hints.
* @param string $relation_type The relation type the URLs are printed for.
* @return array Difference betwen the two arrays.
*/
function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
  if ( 'dns-prefetch' == $relation_type ) {
    /** This filter is documented in wp-includes/formatting.php */
    $emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );

    $urls = array_diff( $urls, array( $emoji_svg_url ) );
  }

  return $urls;
}

function __default_local_avatar()
{
    // this assumes default_avatar.png is in wp-content/themes/active-theme/images
    return get_template_directory_uri() . '/assets/img/user.png';
}
add_filter( 'pre_option_avatar_default', '__default_local_avatar' );

/* Hide WP version strings from scripts and styles
 * @return {string} $src
 * @filter script_loader_src
 * @filter style_loader_src
 */
function remove_wp_version_strings( $src ) {
 global $wp_version;
 parse_str(parse_url($src, PHP_URL_QUERY), $query);
 if ( !empty($query['ver']) && $query['ver'] === $wp_version ) {
  $src = remove_query_arg('ver', $src);
}
return $src;
}
add_filter( 'script_loader_src', 'remove_wp_version_strings' );
add_filter( 'style_loader_src', 'remove_wp_version_strings' );

/* Hide WP version strings from generator meta tag */
function wpmudev_remove_version() {
  return '';
}
add_filter('the_generator', 'wpmudev_remove_version');

// WP REST API requiring authentication
add_filter( 'rest_authentication_errors', function( $result ) {
  if ( ! empty( $result ) ) {
    return $result;
  }
  if ( ! is_user_logged_in() ) {
    return new WP_Error( 'rest_not_logged_in', 'You are not currently logged in.', array( 'status' => 401 ) );
  }
  return $result;
});