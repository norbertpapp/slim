  <article id="post-<?php the_ID(); ?>" <?php post_class( 'middle' ); ?>>
    <header>
      <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><h1><?php the_title(); ?></h1></a>
    </header>
    <?php
    if ( has_post_thumbnail() ) { ?>
      <figure class="featured-image">
        <?php the_post_thumbnail( 'large' ); ?>
      </figure>
      <?php
    }
    ?>
    <div class="post-content">
      <?php the_content(); ?>
    </div>
  </article>