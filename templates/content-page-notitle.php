  <article id="post-<?php the_ID(); ?>" <?php post_class( 'middle' ); ?>>
    <div class="post-content">
      <?php the_content(); ?>
    </div>
    <footer>
    </footer>
  </article>